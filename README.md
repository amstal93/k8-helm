# Kubernetes Helm

[![](https://images.microbadger.com/badges/image/mdeterman/k8-helm.svg)](https://microbadger.com/images/mdeterman/k8-helm "Get your own image badge on microbadger.com")
[![](https://images.microbadger.com/badges/version/mdeterman/k8-helm.svg)](https://microbadger.com/images/mdeterman/k8-helm "Get your own version badge on microbadger.com")

## Overview
This container provides the Helm client for use with Kubernetes

## Supported tags and respective `Dockerfile` links

- v2.13.1, latest [(v2.13.1/Dockerfile)](https://gitlab.com/io_determan/docker/k8-helm/blob/v2.13.1/Dockerfile)
- v2.13.0 [(v2.13.0/Dockerfile)](https://gitlab.com/io_determan/docker/k8-helm/blob/v2.13.0/Dockerfile)
- v2.13.0-rc.1 [(v2.13.0-rc.1/Dockerfile)](https://gitlab.com/io_determan/docker/k8-helm/blob/v2.13.0-rc.1/Dockerfile)
- v2.12.3 [(v2.12.0/Dockerfile)](https://gitlab.com/io_determan/docker/k8-helm/blob/v2.12.3/Dockerfile)
- v2.12.2 [(v2.12.0/Dockerfile)](https://gitlab.com/io_determan/docker/k8-helm/blob/v2.12.2/Dockerfile)
- v2.12.1, [(v2.12.0/Dockerfile)](https://gitlab.com/io_determan/docker/k8-helm/blob/v2.12.1/Dockerfile)
- v2.12.0 [(v2.12.0/Dockerfile)](https://gitlab.com/io_determan/docker/k8-helm/blob/v2.12.0/Dockerfile)
- v2.11.0, [(v2.11.0/Dockerfile)](https://gitlab.com/io_determan/docker/k8-helm/blob/v2.11.0/Dockerfile)
- v2.10.0, [(v2.10.0/Dockerfile)](https://gitlab.com/io_determan/docker/k8-helm/blob/v2.10.0/Dockerfile)
- v2.9.1, [(v2.9.1/Dockerfile)](https://gitlab.com/io_determan/docker/k8-helm/blob/v2.9.1/Dockerfile)